import React, {PureComponent} from 'react';
import './TodoApp.sass';
import {exampleTodos} from './data';
import TodoList from "./components/TodoList/TodoList";
import AppBar from "./components/AppBar/AppBar";


export interface ITodo {
    id: string,
    name: string,
    isChecked: boolean,
}

interface IState {
    newTaskText: string,
    todos: ITodo[]
}


class TodoApp extends PureComponent {
    state: IState

    constructor(props: {}) {
        super(props);

        this.state = {
            newTaskText: 'Type new task',
            todos: exampleTodos,
        }

        this.changeInput = this.changeInput.bind(this);
        this.selectTodo = this.selectTodo.bind(this);
        this.selectAllCheckbox = this.selectAllCheckbox.bind(this);
        this.addTodo = this.addTodo.bind(this);
        this.editTodo = this.editTodo.bind(this);
        this.deleteSelectedTodos = this.deleteSelectedTodos.bind(this);
    }

    changeInput(e: React.FormEvent<HTMLInputElement>) {
        this.setState({
            newTaskText: e.currentTarget.value
        })
    }

    selectTodo(e: React.FormEvent<HTMLInputElement>): void {
        const todosCopy = this.state.todos.map((todo: ITodo) =>
            todo.id === e.currentTarget.id ? Object.assign({}, todo, {isChecked: !todo.isChecked}) : todo
        )

        this.setState({
            todos: todosCopy
        })
    }

    selectAllCheckbox(): void {
        if (this.state.todos.length) {
            const todosCopy = this.state.todos.map((todo: ITodo) => (
                Object.assign({}, todo, {isChecked: true})
            ))
            this.setState({
                todos: todosCopy
            })
        }
    }

    addTodo(): void {
        this.setState({
            todos: [{
                id: Date().valueOf() + Math.random(),
                name: this.state.newTaskText,
                isChecked: false
            }, ...this.state.todos]
        })
        this.setState({newTaskText: ''})
    }

    editTodo(e: React.FormEvent<HTMLInputElement>): void {
        const target = e.currentTarget

        const todosCopy = this.state.todos.map((todo: ITodo) => (
            todo.id === target.id ? Object.assign({}, todo, {name: [target.value]}) : todo)
        )

        this.setState({
            todos: todosCopy
        })
    }

    deleteSelectedTodos(): void {
        if (this.state.todos.length) {
            const todosCopy = this.state.todos.filter(todo => !todo.isChecked)

            this.setState({
                todos: todosCopy
            })
        }
    }


    render() {
        return (
            <div className='App'>
                <div className='app__container'>
                    <AppBar newTaskText={this.state.newTaskText}
                            changeInput={this.changeInput}
                            addTodo={this.addTodo}
                            selectAllCheckbox={this.selectAllCheckbox}
                            deleteSelectedTodos={this.deleteSelectedTodos}
                    />
                    {
                        this.state.todos.length ?
                            <TodoList todos={this.state.todos}
                                      selectTodo={this.selectTodo}
                                      editTodo={this.editTodo}
                            /> : ''
                    }
                </div>
            </div>
        )
    }

}

export default React.memo(TodoApp);





