import React, {MouseEventHandler, PureComponent} from "react";
import './Button.sass';


interface ButtonProps {
    onClick: MouseEventHandler<HTMLButtonElement>,
    content: React.ReactNode,
}

class Button extends PureComponent<ButtonProps> {
    render() {
        return (
            <button onClick={this.props.onClick}>
                {this.props.content}
            </button>
        )
    }
}

export default Button;





