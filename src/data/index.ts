export const exampleTodos = [
    {
        id: Date().valueOf(),
        name: 'Test testing of test the testers are testing.',
        isChecked: true,
    },
    {
        id: Date().valueOf() + "45",
        name: 'Test task 2: This is a bad approach because React will never.',
        isChecked: false,
    }
]