import React, {PureComponent} from "react";
import './TodoCard.sass';


interface TodoCardProps {
    id: any,
    checked: boolean,
    selectTodo: (e: React.FormEvent<HTMLInputElement>) => void,
    value: string,
    editTodo: (e: React.FormEvent<HTMLInputElement>) => void,
}

class TodoCard extends PureComponent<TodoCardProps> {
    render() {
        return (
            <div className='todo'>
                <input type='checkbox'
                       id={this.props.id}
                       checked={this.props.checked}
                       onChange={this.props.selectTodo}
                />
                <input className='todo-content'
                       id={this.props.id}
                       value={this.props.value}
                       onChange={this.props.editTodo}
                />
            </div>
        )
    }
}

export default TodoCard;





