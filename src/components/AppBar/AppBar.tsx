import React, {PureComponent} from 'react';
import './AppBar.sass';
import Button from "../../primitives/Button/Button";


interface AppBarProps {
    newTaskText: string,
    changeInput: (e: React.FormEvent<HTMLInputElement>) => void,
    addTodo: () => void,
    selectAllCheckbox: () => void,
    deleteSelectedTodos: () => void,
}


class AppBar extends PureComponent<AppBarProps> {
    constructor(props: AppBarProps) {
        super(props);
    }

    render() {
        return (
            <div className='func-line__container'>
                <div>
                    <input value={this.props.newTaskText} onChange={this.props.changeInput}/>
                    <Button content='Add' onClick={this.props.addTodo}/>
                </div>
                <div>
                    <p>All functions:</p>
                    <div className='func-buttons__container'>
                        <Button content='All' onClick={this.props.selectAllCheckbox}/>
                        <Button content='Delete' onClick={this.props.deleteSelectedTodos}/>
                    </div>
                </div>
            </div>
        )
    }

}

export default AppBar;





