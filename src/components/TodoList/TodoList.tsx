import React, {PureComponent} from 'react';
import './TodoList.sass';
import {ITodo} from "../../TodoApp";
import TodoCard from "../TodoCard/TodoCard";


interface TodoListProps {
    todos: ITodo[],
    selectTodo: (e: React.FormEvent<HTMLInputElement>) => void,
    editTodo: (e: React.FormEvent<HTMLInputElement>) => void,
}


class TodoList extends PureComponent<TodoListProps> {
    constructor(props: TodoListProps) {
        super(props);
    }

    render() {
        return (
            <div className="todo-list">
                {
                    this.props.todos.map((todo: ITodo, id: number) =>
                        <TodoCard key={id}
                                  id={todo.id}
                                  checked={todo.isChecked}
                                  selectTodo={this.props.selectTodo}
                                  value={todo.name}
                                  editTodo={this.props.editTodo}/>
                    )
                }
            </div>
        )
    }

}

export default TodoList;





